#include <iostream>

using namespace std;

constexpr long double PI = 3.141592653589793238462643383279502884197;

constexpr long double deg_2_rad(int deg){

    return PI * static_cast<long double>(deg) / static_cast<long double>(180.0);
    
}

int main(){
    cout << "deg_2_rad(30) = " << deg_2_rad(30) << endl;
    cout << "deg_2_rad(90) = " << deg_2_rad(90) << endl;
    cout << "deg_2_rad(150) = " << deg_2_rad(150) << endl;

    int angle;

    cout << "Angle to convert: "; cin >> angle;

    cout << "deg_2_rad(" << angle << ") = " << deg_2_rad(angle) << endl;


    return 0;
}