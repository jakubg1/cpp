#include <iostream>

using namespace std;

int main(){

    int wielkosc;

    while(true){
        cout << "Podaj wielkosc tablicy: "; cin >> wielkosc; cout << endl;

        if(wielkosc == 0) break;
        else{
            int *pointerTab = new int[wielkosc];

            for (int i=0; i<wielkosc; i++) 
            {
                pointerTab[i] = i+5*i;
                cout << " " << pointerTab[i];
            }
        }
    }

    return 0;
}