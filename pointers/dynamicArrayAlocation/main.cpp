#include <iostream>

using namespace std;

int main(){

    int sizeOfArray{1};

    cout << "Enter the size of the array: "; cin >> sizeOfArray;


    int *arrayPointer = new int[sizeOfArray];

    for(int i = 0; i < sizeOfArray; i++){
        arrayPointer[i] = i * 21 + 113 * (i+1);
    }


    for(int i = 0; i < sizeOfArray; i++){
        cout << *(arrayPointer + i) << endl;
        // cout << arrayPointer[i] << endl; // (the same)
    }


    return 0;
}