#include <iostream>

using namespace std;

int wrongLevelEntered();
int welcomeAtFirstHall();
int welcomeAtSecondHall();

void sayWelcome(int (*appriopriateFunction)());


int main(){

    int level;

    cout << "Enter at which level will you watch a movie? : "; 
    cin >> level;

    int (*chosenFunction)();

    if (level == 1) chosenFunction = &welcomeAtFirstHall;
    else if (level == 2) chosenFunction = &welcomeAtSecondHall;
    else chosenFunction = &wrongLevelEntered;

    sayWelcome(chosenFunction);

    return 0;
}

int welcomeAtFirstHall(){
    cout << "-- Welcome at first Hall -- " << endl;
    return 1;
}

int welcomeAtSecondHall(){
    cout << " -- Welcome at second Hall -- " << endl;
    return 2;
}

int wrongLevelEntered(){
    cout << " -- Welcome at none Hall -- " << endl;
    return 0;
}

void sayWelcome(int (*appriopriateFunction)()){
    int number = appriopriateFunction();
    if (number != 0) cout << "CURRENT_LEVEL: " << number << endl;
    else cout << "CURRENT_LEVEL: YOU ARE NOWHERE" << endl;
}