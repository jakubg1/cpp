#include <iostream>

using namespace std;

int main(){

    int numberOfBuses = 12;
    int numberOfTrains = 10;

    const int * pointer = &numberOfBuses;

    cout << *pointer << endl;

    // *pointer = 13; // ERROR!!!


    pointer = &numberOfTrains;

    cout << *pointer << endl;

    return 0;
}