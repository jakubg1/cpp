#include <iostream>

using namespace std;

int main(){

    const int myVariable = 15;

    const int *myPointer = &myVariable;

    cout << "myVariable przed zmianą: " << *myPointer << endl;

    // myPointer = 89; // ERROR

    int *myNewPointer = const_cast<int *>(myPointer);

    *myNewPointer = 89;

    cout << "myVariable po zmianie: " << *myPointer << endl;

    return 0;
}