#include <iostream>
#include <string>

using namespace std;

void showHall(bool hall[10][10], int rows, int cols);

bool reservateSeat(bool hall[10][10], int row, int col);

int main(){

    bool halls[4][10][10]{};

    string movie = "";
    int seatsToChange = 0, currentHall = -1;
    cout << "Our movies: " << endl 
        << "Indiana Jones" << endl
        << "Star Wars" << endl 
        << "Thor" << endl
        << "Mr Robot" << endl;
    
    cout << "Which movie do you want to watch? (type anything to exit): "; getline(cin, movie);

    cin.clear();

    cout << endl << "You chose " << movie << endl;

    cout << "How many seats do you want to reserve? : "; cin >> seatsToChange;

    cin.clear();

    if(movie == "Indiana Jones") currentHall = 0;
    else if(movie == "Star Wars") currentHall = 1;
    else if(movie == "Thor") currentHall = 2;
    else if(movie == "Mr Robot") currentHall = 3;

    
    while (seatsToChange-- > 0){
        bool result = false;
        do{
            int row, col;
            cout << "Enter the row number: "; cin >> row;
            cout << endl;
            cout << "Enter the column number: "; cin >> col;

            result = reservateSeat(halls[currentHall], row, col);

        }while(!result);
    }

    cout << "Indiana Jones" << endl; showHall(halls[0], 10, 10);
    cout << "Star Wars" << endl; showHall(halls[1], 10, 10);
    cout << "Thor" << endl; showHall(halls[2], 10, 10);
    cout << "Mr Robot" << endl; showHall(halls[3], 10, 10);

    return 0;
}


void showHall(bool hall[10][10], int rows, int cols){
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            cout << hall[i][j];
        }
        cout << endl;
    }
}

bool reservateSeat(bool hall[10][10], int row, int col){
    if (hall[row][col] == 0){
        hall[row][col] = 1;
        return true;
    } else return false;
}