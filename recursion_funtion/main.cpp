#include <iostream>

using namespace std;

void in_binary_system(int number){
    if (number > 0) {
        in_binary_system((number - (number % 2)) / 2);
        cout << number % 2;
    }
}

int main(){

    short int input;

    cout << "Number to convert to bin system: "; cin >> input;

    in_binary_system(input);

    cout << endl;

    return 0;
}