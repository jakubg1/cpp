#include <iostream>

#define MAX(a,b) (a)>(b)?(a):(b)
#define PRINTOUT(text) cout<<#text<<" = "<<text<<endl;

using namespace std;

int main() {
    int x = 4, y = 22;

    int maximum = MAX(x,y);
    PRINTOUT(maximum)

    return 0;
}