#include <iostream>
#include <string>
#define JOINING_STRINGS(name, value) name ## value

using namespace std;

int main(){


    int JOINING_STRINGS(marian_banas, _login_counter) = 0;
    int JOINING_STRINGS(jan_kowal, _login_counter) = 0;
    int JOINING_STRINGS(janusz_rewinski, _login_counter) = 0;

    int JOINING_STRINGS(marian_banas, _life_amout) = 1;
    int JOINING_STRINGS(jan_kowal, _life_amout) = 1;
    int JOINING_STRINGS(janusz_rewinski, _life_amout) = 1;

    JOINING_STRINGS(marian_banas, _login_counter)++;
    JOINING_STRINGS(marian_banas, _login_counter)++;
    JOINING_STRINGS(janusz_rewinski, _login_counter)++;
    JOINING_STRINGS(marian_banas, _login_counter)++;
    JOINING_STRINGS(jan_kowal, _life_amout) += 5;

    cout << "Jan Kowal counter: " << JOINING_STRINGS(marian_banas, _login_counter) << endl;
    cout << "Marian Banas counter: " << JOINING_STRINGS(janusz_rewinski, _login_counter) << endl;
    cout << "Janusz Rewinski counter: " << JOINING_STRINGS(jan_kowal, _login_counter) << endl;

    cout << "Jan Kowal counter: " << JOINING_STRINGS(marian_banas, _life_amout) << endl;
    cout << "Marian Banas counter: " << JOINING_STRINGS(janusz_rewinski, _life_amout) << endl;
    cout << "Janusz Rewinski counter: " << JOINING_STRINGS(jan_kowal, _life_amout) << endl;

    return 0;
}