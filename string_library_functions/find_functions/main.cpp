#include <iostream>
#include <string>

using namespace std;

int main(){

    string my_string{"Czesc, gram teraz w CS GO. Wczesniej mowilem, ze sram, bo bylem w kiblu. Tak, mam pieniadze, spoko."};

    cout << "Pierwszy podciag 'am' znajduje się na pozycji: " << my_string.find("am") << endl;
    cout << "Ostatni podciag 'am' znajduje się na pozycji: " << my_string.rfind("am") << endl;
    cout << "Pierwsza z liter 's', 'z', 'f', 'g', 'h' oraz 't' znajduje się na pozycji: " << my_string.find_first_of("szfght") << endl;
    cout << "Ostatnia z liter 's', 'z', 'f', 'g', 'h' oraz 't' znajduje się na pozycji: " << my_string.find_last_of("szfght") << endl;
    cout << "Ostatnia kropka znajduje się na pozycji " << my_string.find_last_of(".") << " zatem ciag ma " << my_string.find_last_of(".") + 1 << " znakow" << endl;

    return 0;
}