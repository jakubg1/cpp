#include <iostream>
#include <vector>
#include <string>

using namespace std;

void showArea(vector<vector<char>> area);

void sow(vector<vector<char>> & area, string plant, int quantity, int row, int col);

int main() {


    vector<vector<char>> myArea = {
        10,
        vector<char>(10,'O')
    };

    cout << "Witaj na farmie! Oto Twoje pole: " << endl;

    showArea(myArea);

    while(true){
        string plant = "";
        int quantity = 0, row = 0, col = 0;

        cout << "Co chcesz posiac (Z - zboze, K - kukurydza): "; cin >> plant;
        cout << "Ile pol chcesz obsiac: "; cin >> quantity;
        cout << "Od jakiego rzadu chcesz obsiac: "; cin >> row;
        cout << "Od jakiej kolumny chcesz obsiac: "; cin >> col;
        if(plant == "Z" || plant == "K"){
            sow(myArea, plant, quantity, row, col);
        }

        showArea(myArea);
    }
    

    return 0;
}

void showArea(vector<vector<char>> area){
    for(int i = 0; i < area.size(); i++){
        for(int j = 0; j < area[i].size(); j++){
            cout << "|" << area[i][j];
        }
        cout << "|" << endl;
    }
}

void sow(vector<vector<char>> & area, string plant, int quantity, int row, int col){
    while(row < area.size() && quantity > 0){
        while(col < area[row].size() && quantity > 0){
            if(area[row][col] == 'O') {
                quantity--;
                area[row][col] = plant[0];
            }
            col++;
        }
        row++;
        col = 0;
    }
}